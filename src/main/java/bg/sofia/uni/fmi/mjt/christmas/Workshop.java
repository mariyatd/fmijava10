package bg.sofia.uni.fmi.mjt.christmas;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Workshop {

    private static final int TOTAL_ELVES = 20;
    private BlockingQueue<Gift> backlog;
    private AtomicInteger wishCounter = new AtomicInteger(0);
    private ExecutorService pool;
    private Elf[] elves = new Elf[TOTAL_ELVES];
    private boolean isChristmas = false;

    public Workshop() {
        this.backlog = new LinkedBlockingQueue<>();
        this.pool = Executors.newFixedThreadPool(TOTAL_ELVES);

        for (int i = 0; i < TOTAL_ELVES; i++) {
            Elf elf = new Elf(i, this);
            elves[i] = elf;
            pool.submit(elf);
        }
    }

    /**
     * Adds a gift to the elves' backlog.
     **/
    public void postWish(Gift gift) {
        wishCounter.incrementAndGet();
        backlog.add(gift);
        backlog.add(null);
    }

    /**
     * Returns an array of the elves working in Santa's workshop.
     **/
    public Elf[] getElves() {
        return elves;
    }

    /**
     * Returns the next gift from the elves' backlog that has to be manufactured.
     **/
    public Gift nextGift() throws InterruptedException {
        return backlog.take();
    }

    /**
     * Returns the total number of wishes sent to Santa's workshop by the kids.
     **/
    public int getWishCount() {
        return wishCounter.get();
    }

    /**
     * Gets Christmas state.
     **/
    public boolean getChristmas() {
        return isChristmas;
    }

    public boolean isBacklogEmpty() {
        return backlog.isEmpty();
    }

    /**
     * Sets Christmas.
     **/
    public void setChristmas(boolean isChristmas) {
        this.isChristmas = isChristmas;
    }

    public void shutdownThreadPool() {
        pool.shutdown();
    }
}

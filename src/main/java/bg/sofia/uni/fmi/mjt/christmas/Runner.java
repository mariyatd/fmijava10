package bg.sofia.uni.fmi.mjt.christmas;

public class Runner {

    public static void main(String[] args) {
        final int KID_COUNT = 100;
        Workshop workshop = new Workshop();
        Thread[] kids = new Thread[KID_COUNT];

        for (int k = 0; k < KID_COUNT; k ++) {
            Thread t = new Thread(new Kid(workshop));
            kids[k] = t;
            t.start();
        }

        for (int k = 0; k < KID_COUNT; k ++) {
            try {
                kids[k].join();
            } catch (InterruptedException e) {
                throw new RuntimeException();
            }
        }

        workshop.setChristmas(true);

        System.out.println("f");
    }
}

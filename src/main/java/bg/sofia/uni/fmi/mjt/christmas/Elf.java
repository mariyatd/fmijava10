package bg.sofia.uni.fmi.mjt.christmas;

import java.util.concurrent.atomic.AtomicInteger;

public class Elf implements Runnable {

    private Workshop workshop;
    private int elfId;
    private volatile AtomicInteger giftsProduced = new AtomicInteger(0);

    public Elf(int id, Workshop workshop) {
        this.workshop = workshop;
        this.elfId = id;
    }

    /**
     * Gets a wish from the backlog and creates the wanted gift.
     **/
    public void craftGift() throws InterruptedException {

        while (true) {
            if (workshop.getChristmas() && workshop.isBacklogEmpty()) {
                workshop.shutdownThreadPool();
                return;
            }

            Gift giftToProduce = workshop.nextGift();

            try {
                Thread.sleep(giftToProduce.getCraftTime());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            giftsProduced.incrementAndGet();
        }
    }

    /**
     * Returns the total number of gifts that the given elf has crafted.
     **/
    public int getTotalGiftsCrafted() {
        return giftsProduced.intValue();
    }

    @Override
    public void run() {
        System.out.println("elf" + elfId);
        try {
            craftGift();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

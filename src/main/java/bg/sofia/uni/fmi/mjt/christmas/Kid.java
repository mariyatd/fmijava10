package bg.sofia.uni.fmi.mjt.christmas;

import java.util.Random;

public class Kid implements Runnable {

    public static final int MAX_THINKING = 20;
    private Workshop workshop;

    public Kid(Workshop workshop) {
        this.workshop = workshop;
    }

    @Override
    public void run() {
        Random random = new Random();
        int time = random.nextInt(MAX_THINKING);

        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        workshop.postWish(Gift.getGift());
    }
}